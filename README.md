# Mesa performance tracking

## Introduction

This repository constains scripts to acquire data about the performance of Mesa drivers and also of Mesa's CI, and present it in Grafana.

It is composed of three parts:

- Scripts for the data acquisition and storage in InfluxDB
- Definition of Grafana dashboards in YAML
- A gitlab-ci pipeline for running these scripts preiodically and keeping the dashboards in Grafana up-to-date

After changes in production, the updated dashboards can be seen at:

- https://grafana.freedesktop.org/d/uINdWI3Mz/mesa-driver-performance
- https://grafana.freedesktop.org/d/-UCa0si7z/mesa-ci-stats

## Testing

A Docker compose definition is provided so contributors can test their changes locally before submitting their changes for review.

Steps:

1. Create a personal access token for your user with the API scope and set it as a masked variable of your fork with the name GITLAB_TOKEN
1. If you want to run reports generation in your fork, set the variables accordingly:
    - MESA_CI_STATUS_SEND_REPORT
    - MESA_CI_STATUS_ARGS
    - MESA_CI_ALERTS_SEND_REPORT
    - PERFORMANCE_ALERT_ARGS
1. Disable shared runners in the user's fork repo
1. Build and run the containers containing Grafana, InfluxDB and gitlab-runner, specifying the runner registration token for your fork:
   ```$ RUNNER_REGISTRATION_TOKEN=[enter your token here] docker-compose up```
1. Check that you can log into the Grafana service by going to http://localhost:3000
1. Execute a pipeline in your fork
1. After the pipeline finishes, test your changes in http://localhost:3000

## Dashboard deployment

We are deprecating `grafana-dashboard-builder` in favor of `dashboard-exporter`.
It supports creating and editing dashboards as a code.

Currently, only the "Mesa Driver Performance" dashboard is using the new tool.
Eventually, the other ones will migrate to the new one as well.

### Updating dashboards

1. Update dashboard title, datasource name, dashboard uid etc by editing
`dashboards/metadata.yml`
2. Update the JSON model by overwriting an existing JSON file inside
`./dashboards` and ensure that it is listed in `metadata.yml`
### Creating dashboards
1. Open any dashboard in fd.o [Grafana instance](https://ci-stats-grafana.freedesktop.org).
1. Copy the JSON model to a file and put it in the `./dashboards` folder
   - You have to be an admin or the dashboard's author
   - You can find it by appending: `&editview=dashboard_json` to the dashboard URL
1. Update `dashboards/metadata.yml` the same way as the other dashboards
   - Example:
   ```
   dashboard_title:
    json_model: model.json

    # overrides are optional
    # it is more used for migrating data easily
    overrides:
      uid: myid
      datasource:
        uid: mydb
   ```
1. Run the dashboard exporter script:
   ```
   dashboard_exporter/exporter.py create_or_update dashboards/metadata.yml
   ```
1. Check if the new dashboard appears into the dashboards General folder, which
is available at `/dashboards` endpoint
